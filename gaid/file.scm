(define-module (gaid file)
  #:use-module (gaid records)
  #:export (file
            make-file
            file?
            this-file
            file-location))

(define-record-type* <file>
  file make-file
  file?
  this-file
  (location file-location))
