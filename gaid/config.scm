(define-module (gaid config)
  #:use-module (gaid records)
  #:export (config
            make-config
            config?
            this-config
            config-name
            config-source
            config-build
            config-method
            config-destination))

(define-record-type* <config>
  config make-config
  config?
  this-config
  (name config-name)
  (source config-source)
  (build config-build (default #f))
  (method config-method (default 'symlink))
  (destination config-destination))
