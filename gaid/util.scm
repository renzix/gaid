(define-module (gaid util)
  #:use-module (ice-9 readline)
  #:use-module (ice-9 format)
  #:use-module (gaid config)
  #:use-module (gaid file))

(define-public ask-to-overide
  (lambda (dest)
    (let ((input (readline (format #f "Would you like to overide ~s (y/n): " dest))))
      (string=? "y" input))))

(define-public install
  (lambda (conf)
    ;; cond for type - @TODO(Renzix)
    (let ((method (config-method conf)))
      (cond
       ((eq? method 'copy)
        (if (access? (config-destination conf) F_OK)
            (when (ask-to-overide (config-destination conf))
              (delete-file (config-destination conf))
              (copy-file (file-location (config-source conf)) (config-destination conf)))
            (copy-file (file-location (config-source conf)) (config-destination conf))))))))

(define-public remove
  (lambda (conf)
    ;;cond for type - @TODO(Renzix)
    (let ((files (config-destination conf)))
      (when (access? (config-destination conf) F_OK)
        (delete-file files)))))

(define-public use
  (lambda (conf use-or-delete)
    (if use-or-delete
        (install conf)
        (remove conf))))
