(define-module (formats vimscript)
  #:use-module (formats vimscript plugin)
  #:use-module (gaid file))

(define-public set
  (lambda* (varname #:optional value)
    (if value
        (format #f "set ~a=~a" varname value)
        (format #f "set ~a" varname))))

(define-public let
  (lambda (varname value)
    (format #f "set ~a=~a" varname value)))

;; (define-public plugin-manager nil)
(define plugin-list '())
(define-public plugin
  (lambda* (name #:key
                 (after '()) (init '()) (config '()))
    (append! plugin-list name))) ;; @TODO(Renzix): Make class which gets appended instead of name

(define-public color
  (lambda (value)
    (string-append "color ~a" value)))

;; for now just read to tmp file - @SPEED(Renzix)
(define-public (create . statements)
  (let* ((loc "/tmp/gaid.tmp")
         (f (open-file loc "w")))
    (for-each
     (lambda (statement)
       ;; @TODO(Renzix): Test to see statement type and do thing accordingly
       (display statement f)
       (newline f))
     statements)
    (close f)
    (file (location loc))))
