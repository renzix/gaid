(define-module (gaid main)
  #:use-module (gaid config)
  #:use-module (gaid file)
  #:use-module (gaid util))

;; USER CONFIG
(define home (getenv "HOME"))
(define dotfiles (string-append home "/Dotfiles"))

(use-modules ((formats vimscript) #:prefix vs:))
(define neovim
  (config
   (name "neovim")
   (source
    ;; Alternatively you can just use a copy, symlink or any other way to generate a file
    (vs:create
     ;; (vs:plugin-manager "vim-plug")
     (vs:plugin "airblade/vim-gitgutter")
     (vs:plugin "airblade/vim-rooter")

     (vs:plugin "skywind3000/asyncrun.vim")
     (vs:plugin "dracula/vim"
                #:config (vs:color "dracula"))
     (vs:plugin "kana/vim-textobj-entire")
     (vs:plugin "kana/vim-textobj-user"
                #:after "kana/vim-textobj-entire") ; Can take a list or multiple items
     (vs:set "foldmethod" "marker")
     ;; Change Color when entering Insert Mode
     ;; (vs:autocmd "InsertEnter" "*" (vs:highlight "CursorLine" :ctermbg "Green" :ctermfg "Red"))
     ;; Revert Color to default when leaving Insert Mode
     ;; (vs:autocmd "InsertLeave" "*" (vs:highlight "CursorLine" :ctermbg "Yellow" :ctermfg "None"))
     ;; Highlight tabs
     ;; (vs:exec "set listchars=tab:\uBB\uBB")
     (vs:set "list")
     ;; (vs:match "ErrorMsg" "/\t/")
     ;; Highlight Trailing whitespace
     ;; (vs:highlight "ColorColumn" :ctermbg "darkred")
     ;; (vs:command "Cfg" ":e $MYVIMRC")
     ;; (vs:map
     ;;  :nv "Q"  "@q"
     ;;  :n  "g=" "magg=G`a"
     ;;  :nv "Y"  "y$")
     ))
   (method 'copy)
   (destination (string-append home "/.config/nvim/init.vim"))))

;; ACTUAL INSTALLATIONS
(use neovim #t)
